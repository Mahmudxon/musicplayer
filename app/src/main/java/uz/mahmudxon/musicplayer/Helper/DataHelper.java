package uz.mahmudxon.musicplayer.Helper;

import android.graphics.Bitmap;

import java.util.ArrayList;

import uz.mahmudxon.musicplayer.Models.Album;
import uz.mahmudxon.musicplayer.Models.Artist;
import uz.mahmudxon.musicplayer.Models.Genre;
import uz.mahmudxon.musicplayer.Models.PageModel;
import uz.mahmudxon.musicplayer.Models.Playlist;
import uz.mahmudxon.musicplayer.Models.Song;

public class DataHelper {
    public static ArrayList<Song> songs;
    public static ArrayList<Album> albums;
    public static ArrayList<Artist> artists;
    public static ArrayList<Genre> genres;
    public static ArrayList<Playlist> playlists;
    public static  String currentUrl = "";



   public static Bitmap getAlbumImg(int id)
    {
        Bitmap img = null;

        for (int i = 0; i<albums.size(); i++)
            if(albums.get(i).getId() == id)
                img = albums.get(i).getImg();

        return img;
    }




    private DataHelper()
    {
        // not object this class
    }
}
