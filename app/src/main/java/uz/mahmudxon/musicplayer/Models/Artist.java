package uz.mahmudxon.musicplayer.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;



import java.util.ArrayList;

import uz.mahmudxon.musicplayer.R;

public class Artist {


int id;
String name;
int countSongs;

    public Artist(int id, String name, int countSongs) {
        this.id = id;
        this.name = name;
        this.countSongs = countSongs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountSongs() {
        return countSongs;
    }

    public void setCountSongs(int countSongs) {
        this.countSongs = countSongs;
    }
}
