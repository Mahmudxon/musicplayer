package uz.mahmudxon.musicplayer.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Genre {
    public final int id;
    public final String name;
    public final int songCount;

    public Genre(int id, String name, int songCount) {
        this.id = id;
        this.name = name;
        this.songCount = songCount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSongCount() {
        return songCount;
    }
}
