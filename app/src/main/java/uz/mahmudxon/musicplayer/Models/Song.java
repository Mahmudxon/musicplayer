package uz.mahmudxon.musicplayer.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Song  {

    public final int id;
    public final String title;
    public final int year;
    public final long duration;
    public final String data;
    public final int albumId;
    public final String albumName;
    public final int artistId;
    public final String artistName;

    public Song(int id, String title, int year, long duration, String data, int albumId, String albumName, int artistId, String artistName) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.data = data;
        this.albumId = albumId;
        this.albumName = albumName;
        this.artistId = artistId;
        this.artistName = artistName;
    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public long getDuration() {
        return duration;
    }

    public String getData() {
        return data;
    }

    public int getAlbumId() {
        return albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public int getArtistId() {
        return artistId;
    }

    public String getArtistName() {
        return artistName;
    }
}
