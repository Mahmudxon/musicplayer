package uz.mahmudxon.musicplayer.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public class Album {

    int id;
    String name;
    String artist;
    Bitmap img;
    int countSongs;

    public Album(int id, String name, String artist, Bitmap img, int countSongs) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.img = img;
        this.countSongs = countSongs;
    }
public Album(int id, String name, String artist, int countSongs) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.img = null;
        this.countSongs = countSongs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public int getCountSongs() {
        return countSongs;
    }

    public void setCountSongs(int countSongs) {
        this.countSongs = countSongs;
    }

}
