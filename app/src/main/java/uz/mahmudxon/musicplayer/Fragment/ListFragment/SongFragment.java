package uz.mahmudxon.musicplayer.Fragment.ListFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uz.mahmudxon.musicplayer.Adapters.SongsAdapter;
import uz.mahmudxon.musicplayer.Helper.DataHelper;
import uz.mahmudxon.musicplayer.Models.Song;
import uz.mahmudxon.musicplayer.R;
import android.app.Service;
import uz.mahmudxon.musicplayer.Service.PlayService;

public class SongFragment extends Fragment {
    RecyclerView recyclerView;
    SongsAdapter adapter;
    OnSongSelected onSongSelected;

    public void setOnSongSelected(OnSongSelected onSongSelected) {
        this.onSongSelected = onSongSelected;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.song_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.list_songs);
        adapter = new SongsAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setOnItemClickListener(new SongsAdapter.OnItemClickListener() {
            @Override
            public void onClick(Song song) {

               if(onSongSelected != null)
                   onSongSelected.onSelect(song);

            }
        });
    }

    public interface OnSongSelected
    {
        void onSelect(Song song);
    }
}
