package uz.mahmudxon.musicplayer.Managers;

import java.util.ArrayList;

import uz.mahmudxon.musicplayer.Models.Album;
import uz.mahmudxon.musicplayer.Models.Artist;
import uz.mahmudxon.musicplayer.Models.Genre;
import uz.mahmudxon.musicplayer.Models.Song;

public class SongManager {

    ArrayList<Song> songs;
    ArrayList<Artist> artists;
    ArrayList<Album> albums;
    ArrayList<Genre> genres;

    public void setData (ArrayList<Song> songs, ArrayList<Artist> artists, ArrayList<Album> albums, ArrayList<Genre> genres) {
        this.songs = songs;
        this.artists = artists;
        this.albums = albums;
        this.genres = genres;
    }
}
