package uz.mahmudxon.musicplayer;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import uz.mahmudxon.musicplayer.Adapters.PagerAdapter;
import uz.mahmudxon.musicplayer.Fragment.ListFragment.AlbumsFragment;
import uz.mahmudxon.musicplayer.Fragment.ListFragment.ArtistsFragment;
import uz.mahmudxon.musicplayer.Fragment.ListFragment.GenresFragment;
import uz.mahmudxon.musicplayer.Fragment.ListFragment.PlaylistFragment;
import uz.mahmudxon.musicplayer.Fragment.ListFragment.SongFragment;
import uz.mahmudxon.musicplayer.Helper.DataHelper;
import uz.mahmudxon.musicplayer.Models.Album;
import uz.mahmudxon.musicplayer.Models.Artist;
import uz.mahmudxon.musicplayer.Models.Genre;
import uz.mahmudxon.musicplayer.Models.PageModel;
import uz.mahmudxon.musicplayer.Models.Song;
import uz.mahmudxon.musicplayer.Service.PlayService;

public class MainActivity extends AppCompatActivity {

    ActionBarDrawerToggle drawerToggle;
    DrawerLayout layout;
    Toolbar toolbar;
    NavigationView navigationView;
    SearchView searchView;
    ViewPager pager;
    ArrayList<PageModel> pages;
    PagerAdapter pagerAdapter;
    TabLayout tabLayout;
    SongFragment songFragment;
    AlbumsFragment albumsFragment;
    GenresFragment genresFragment;
    PlaylistFragment playlistFragment;
    ArtistsFragment artistsFragment;
    int MY_PERMISSION_REQUEST = 0;
    ArrayList<Song> songData;
    ArrayList<Album> albumData;
    ArrayList<Artist> artistData;
    ArrayList<Genre> genreData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        layout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        drawerToggle = new ActionBarDrawerToggle(this, layout, toolbar, R.string.open, R.string.close);
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            // Menyu bosilganda


                return true;
            }
        });
        loadData();
        DataHelper.albums = albumData;
        DataHelper.artists = artistData;
        DataHelper.genres = genreData;
        DataHelper.songs = songData;
        LoadPages();
        songFragment.setOnSongSelected(new SongFragment.OnSongSelected() {
            @Override
            public void onSelect(Song song) {
                DataHelper.currentUrl = song.data;
                startService(new Intent(getApplicationContext(), PlayService.class));
            }
        });
    }

    private void LoadPages() {
        pager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab_layout);
        pages = new ArrayList<>();
        albumsFragment = new AlbumsFragment();
        artistsFragment = new ArtistsFragment();
        genresFragment = new GenresFragment();
        playlistFragment = new PlaylistFragment();
        songFragment = new SongFragment();
        pages.add(new PageModel(getResources().getString(R.string.songs), songFragment));
        pages.add(new PageModel(getResources().getString(R.string.artists), artistsFragment));
        pages.add(new PageModel(getResources().getString(R.string.albums), albumsFragment));
      //  pages.add(new PageModel(getResources().getString(R.string.genre), genresFragment));
        pages.add(new PageModel(getResources().getString(R.string.playlists) , playlistFragment));
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), pages);
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        searchView = (SearchView)menu.findItem(R.id.app_bar_search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                return true;
            }
        });


        return true;
    }

    @Override
    public void onBackPressed() {
        if (this.layout.isDrawerOpen(GravityCompat.START)) {
            this.layout.closeDrawer(GravityCompat.START);
            return;
        }
        if (!searchView.isIconified())
        {
            searchView.setIconified(true);
            return;
        }

        super.onBackPressed();
    }


    private void loadData() {

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }
            else
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }
        }
        else
        {
            getMusic();

        }
    }

    private void getMusic()
    {
        songData = new ArrayList<>();
        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {
            int songId = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int songTitle = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songYear = cursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
            int songDuration = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int songData = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            int songAlbumId = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int songAlbumname = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int songArtistId = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID);
            int songArtistName = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);

            do {

                this.songData.add(new Song(cursor.getInt(songId), cursor.getString(songTitle), cursor.getInt(songYear),
                        cursor.getLong(songDuration), cursor.getString(songData), cursor.getInt(songAlbumId), cursor.getString(songAlbumname),
                        cursor.getInt(songArtistId), cursor.getString(songArtistName)
                        ));

            }
            while (cursor.moveToNext() && !cursor.isAfterLast());
        }

    cursor.close();
    getAlbums();
    }
    private void getAlbums()
    {
        albumData = new ArrayList<>();
        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {

            int albumId = cursor.getColumnIndex(MediaStore.Audio.Albums._ID);
            int albumName = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
            int artist = cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
            int albumArt = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
            int countSongs = cursor.getColumnIndex(MediaStore.Audio.Albums._COUNT);

            do {

            albumData.add(new Album(cursor.getInt(albumId), cursor.getString(albumName), cursor.getString(artist),
                   /*  BitmapFactory.decodeByteArray(cursor.getBlob(albumArt), 0, cursor.getBlob(albumArt).length),*/
                   0));
            }
            while (cursor.moveToNext() && !cursor.isAfterLast());
        }
        cursor.close();
    getArtist();
    }

private void getArtist()
    {
        artistData = new ArrayList<>();
        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {

            int id = cursor.getColumnIndex(MediaStore.Audio.Artists._ID);
            int name = cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
            int count = cursor.getColumnIndex(MediaStore.Audio.Artists._COUNT);

            do {

              artistData.add(new Artist(cursor.getInt(id),
                      cursor.getString(name),
0));

               }
            while (cursor.moveToNext() && !cursor.isAfterLast());
        }
        cursor.close();
        getGenre();
           }

private void getGenre()
    {
        genreData = new ArrayList<>();
        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {

            int id = cursor.getColumnIndex(MediaStore.Audio.Artists._ID);
            int name = cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
            int count = cursor.getColumnIndex(MediaStore.Audio.Artists._COUNT);

            do {

//              genreData.add(new
//                      Genre(cursor.getInt(id),
//                      cursor.getString(name), 0));
//
//
                }
            while (cursor.moveToNext() && !cursor.isAfterLast());
        }

        cursor.close();
           }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == MY_PERMISSION_REQUEST)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                getMusic();
            }
            else
            {
                Toast.makeText(this, "No Perrmission", Toast.LENGTH_SHORT).show();
                finish();
            }

        }

    }


}
