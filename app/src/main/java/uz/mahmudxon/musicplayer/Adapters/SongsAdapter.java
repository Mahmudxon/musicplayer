package uz.mahmudxon.musicplayer.Adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uz.mahmudxon.musicplayer.Helper.DataHelper;
import uz.mahmudxon.musicplayer.Models.Song;
import uz.mahmudxon.musicplayer.R;

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.Viewholder> {
   ArrayList<Song> data;
   OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public SongsAdapter() {
        this.data = DataHelper.songs;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Viewholder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.song_data, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int i) {
            viewholder.BindData(data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView albumart;
        CardView cardView;
        ImageButton btnOption;
        TextView title;
        TextView artist;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            albumart = itemView.findViewById(R.id.song_img);
            cardView = itemView.findViewById(R.id.song_card);
            btnOption = itemView.findViewById(R.id.song_options);
            title = itemView.findViewById(R.id.song_title);
            artist = itemView.findViewById(R.id.song_artist);
        }

        public void BindData(Song song) {
            final Song s = song;
            title.setText(song.title + " - " + song.albumName);
            artist.setText(song.artistName);
            Bitmap bitmap = DataHelper.getAlbumImg(song.albumId);
            if(bitmap != null)
               albumart.setImageBitmap(bitmap);


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener != null)
                        onItemClickListener.onClick(s);
                }
            });
        }
    }
    public interface OnItemClickListener
    {
        void onClick(Song song);
    }
}
