package uz.mahmudxon.musicplayer.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import uz.mahmudxon.musicplayer.Models.PageModel;


public class PagerAdapter extends FragmentPagerAdapter {

    ArrayList<PageModel> data;

    public PagerAdapter(FragmentManager fm, ArrayList<PageModel> data)
    {
        super(fm);
        this.data = data;
    }

    @Override
    public Fragment getItem(int i) {
        return data.get(i).getFragment();

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).getTitle();
    }
}
