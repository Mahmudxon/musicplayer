package uz.mahmudxon.musicplayer.Controller;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentController {
    int layoutId;
    FragmentManager manager;
    FragmentTransaction transaction;
    Fragment lastFragment;

    public FragmentController(@IdRes int layoutId, FragmentManager manager) {
        this.layoutId = layoutId;
        this.manager = manager;
    }

    public  void open (Fragment fragment)
    {
        if(lastFragment != null)
            close();

        transaction = manager.beginTransaction();
        transaction.replace(layoutId, fragment);
        transaction.commit();
        lastFragment = fragment;
    }

    public void close()
    {
        if (lastFragment != null)
        transaction.remove(lastFragment);
        lastFragment = null;
    }
}
