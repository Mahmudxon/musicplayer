package uz.mahmudxon.musicplayer.Service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;

import uz.mahmudxon.musicplayer.Helper.DataHelper;

public class PlayService extends Service {

static MediaPlayer player;
public static boolean isplaying = false;
    @Override
    public void onCreate() {
        if(DataHelper.currentUrl != null){
        player = MediaPlayer.create(getApplicationContext(), Uri.parse(DataHelper.currentUrl));
        player.start();
        isplaying = true;
        }
        else
            stopSelf();
    }


    @Override
    public void onDestroy() {
        if(player.isPlaying())
            player.stop();
        player = null;
        isplaying = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
